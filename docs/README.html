<html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><link rel="stylesheet" id="theme" href="chrome-extension://febilkbfcbhebfnokafefeacimjdckgl/theme/Clearness.css"></head><body><h1>README</h1>
<p>RobotArms is a Entity / Component / Processor<a href="#Processor">*</a> library for <a href="http://unity3d.com">Unity3D</a>. RobotArms emphasizes usage of Unity components as pure data and moves the logic into one or more processors. This data oriented approach leverages Unity's editor to show the full state of your application with no hidden information inside objects. This approach is <strong>not</strong> object oriented but instead takes a more functional-style approach to data processing. If you find that you are often exposing data from a MonoBehaviour just so that <em>another</em> MonoBehaviour can read from it (or worse yet, write to it), RobotArms will be a breath of fresh air.</p>
<p>If you are unfamiliar with this type of system I'd suggest the following reading:</p>
<p><a href="http://en.wikipedia.org/wiki/Entity_component_system">Wikipedia</a><br><a href="http://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-1/">Entity Systems are the future of MMOG development</a><br><a href="http://scottbilas.com/files/2002/gdc_san_jose/game_objects_slides_with_notes.pdf">Original 2002 GDC talk by Scott Bilas</a></p>
<p><a name="Processor"></a>
* Most literature on this topic uses the terms Entity, Component, and System. The usage of the term System has since fallen out of favor somewhat due to the overloaded nature of the term and frequent naming collisions that occur. RobotArms uses the term Processor, so when reading any articles on the topic just substitute "Processor" whenever you see "System".</p>
<p>We were originally inspired to write RobotArms after using the <a href="http://gamadu.com/artemis/">Artemis</a> framework</p>
<h3>Getting started</h3>
<ol>
<li>Get RobotArms<ol>
<li>Download the source and compile</li>
<li>Copy the resulting dlls to your Unity project<ol>
<li>RobotArms.dll goes anywhere in you Assets folder</li>
<li>RobotArmsEditor.dll goes into any folder named Editor in your Assets folder</li>
</ol>
</li>
<li>Alternatively just copy the RobotArms source files to your Unity Assets folder<ol>
<li>Remember to copy both the RobotArms source files and the RobotArmsEditor source files</li>
</ol>
</li>
</ol>
</li>
<li>Start writing Components and Processors<ol>
<li>See the tutorial below</li>
</ol>
</li>
</ol>
<h3>Basic Usage</h3>
<p>The implementation of the steps outlined in this tutorial can be found in the docs/UnityAsteroidsExample folder.</p>
<p>To use RobotArms you will create one or more Components and then create one or more Processors to operate on those components. Let us consider a game like Asteroids and the data we would need.</p>
<ul>
<li>Ship<ul>
<li>Position</li>
<li>Facing (rotation)</li>
<li>Velocity</li>
<li>Turn speed</li>
<li>Fuel</li>
<li>Fuel consumption rate</li>
<li>Thrust force</li>
</ul>
</li>
<li>Player Input<ul>
<li>Keys/buttons for rotation</li>
<li>Key/button for thrusting forward</li>
</ul>
</li>
</ul>
<p>If you were writing this in regular Unity you would probably create a Ship MonoBehaviour that would take care of most of this. You might break these responsibilities into two parts, one for the vectored movement and the other for the ship specific bits if you thought that you might be able to reuse the vectored movement for other objects. It would be pretty tempting to check for input in the Ship MonoBehaviour as that's where you'd be applying thrust and possibly also moving the ship based on its velocity if you didn't opt for a separate vectored movement component. If you did go for 2 components then you'd likely either use GetComponent in your Ship MonoBehaviour to get the VectoredMovement component or you'd have a public field that you could drag/drop the component on to (or you could use [SerialiableField] like you <strong>should</strong> be doing to support a proper OOP style.)</p>
<p>In RobotArms you would definitely want three separate components, one for the movement, one for the ship data, and one for the input. The position is taken care of thanks to Unity's Transform component. This leaves us with just Velocity to track for our movement component. For the ship we have rotation, rotation speed, current fuel (max fuel also if we can gain fuel), fuel consumption rate, and the thrust force. Facing can be retrieved from the Transform using something standard like the forward (z) vector as our facing. In 2d we'd have a similar concept but it would usually be the up (y) vector instead. For player input we could record the raw button presses or map that to something a bit more game specific. In this example I've chosen to go with a more game specific representation.</p>
<p>Here are the components with broken out with the data each one will store</p>
<ul>
<li>VectoredMovement<ul>
<li>Velocity</li>
</ul>
</li>
<li>Ship<ul>
<li>Turn speed</li>
<li>Fuel</li>
<li>Fuel consumption rate</li>
<li>Thrust force</li>
</ul>
</li>
<li>PlayerInput<ul>
<li>Thrusting yes/no</li>
<li>Rotation input as a float representing how much to rotate from -1 = counterclockwise to 1 = clockwise</li>
</ul>
</li>
</ul>
<pre><code class="language-cs python">using UnityEngine;
using RobotArms;

public <span class="class"><span class="keyword">class</span> <span class="title">VectoredMovement</span> :</span> RobotArmsComponent {
    public Vector3 Velocity;
}</code></pre>
<pre><code class="language-cs nginx"><span class="title">using</span> UnityEngine;
<span class="title">using</span> RobotArms;

<span class="title">public</span> class Ship : RobotArmsComponent {
    <span class="title">public</span> float RotationSpeed;
    <span class="title">public</span> float Fuel;
    <span class="title">public</span> float FuelConsumptionPerSecond;
    <span class="title">public</span> float ThrustForce;
}</code></pre>
<pre><code class="language-cs python">using UnityEngine;
using RobotArms;

public <span class="class"><span class="keyword">class</span> <span class="title">PlayerInput</span> :</span> RobotArmsComponent {
    public bool Thrust;
    public float Rotation;
}</code></pre>
<p>RobotArmsComponent is just a subclass of MonoBehaviour that does all the registering of the component with the system for you. Components by themselves don't do anything, so now we can add in a processor to start adding functionality to our game. The simplest element of this game is probably the vectored movement. We simply translate the GameObject based on the velocity vector. This sounds like a VectoredMovementProcessor to me.</p>
<pre><code class="language-cs python">using UnityEngine;
using RobotArms;

[ProcessorOptions(typeof(VectoredMovement))]
public <span class="class"><span class="keyword">class</span> <span class="title">VectoredMovementProcessor</span> :</span> RobotArmsProcessor {
    public override void Process (GameObject entity) {
        var movement = entity.GetComponent&lt;VectoredMovement&gt;();
        entity.transform.Translate(movement.Velocity * Time.deltaTime);
    }
}</code></pre>
<p>Processors declare which GameObjects (aka entities) they wish to receive via the ProcessorOptions attribute. ProcessorOptions is how a processor says "I'm only interested in GameObjects that have the following component(s) attached". ProcessorOptions also allows you to change when a processor is run (Update / FixedUpdate / LateUpdate), as well as specifying a priority so that you can force certain processors to run earlier or later than the rest just like how script execution order works in Unity. Next you need to override either the Process or ProcessAll methods. The Process method is handed one entity at a time while ProcessAll is handed an IEnumerable<gameobject> of all the entities that match the processor's criteria. This second type is useful when you need to make a decision that cuts across all of a certain kind of thing, for example targeting the highest threat enemy. Inside the Process/ProcessAll method you will need to retrieve the component(s) that you want to work with via Unity's GetComponent method. We can rest assured that entity WILL have a VectoredMovement component attached, otherwise it would not have been passed into this processor.</gameobject></p>
<p>Now we don't have a complete system but we do have enough to start testing things out. To get RobotArms working in a scene you'll need 2 things. Create a GameObject to be the ship and attach the VectoredMovement component. Then, create a new empty GameObject and attach the RobotArmsCoordinator component. When you press play nothing will happen unless you have set the values on the Velocity field of the VectoredMovement component to non-zero. Go ahead and adjust the values and you will see the ship begin to move.</p>
<p>Next let's write a processor that reads input and sets the values on the PlayerInput component.</p>
<pre><code class="language-cs python">using UnityEngine;
using RobotArms;

[ProcessorOptions(typeof(PlayerInput))]
public <span class="class"><span class="keyword">class</span> <span class="title">PlayerInputProcessor</span> :</span> RobotArmsProcessor {
    public override void Process (GameObject entity) {
        var input = entity.GetComponent&lt;PlayerInput&gt;();
        input.Thrust = Input.GetAxis(<span class="string">"Vertical"</span>) &gt; <span class="number">0</span>;
        input.Rotation = Input.GetAxis(<span class="string">"Horizontal"</span>);
    }
}</code></pre>
<p>Go ahead and attach the PlayerInput component to your ship GameObject and run the game. If you press the correct inputs (W/A/D, Arrow Keys, Analog Stick on a Gamepad) you should now see the values updating on the component. The last step is now to write a ShipProcessor that uses the data from the Ship and PlayerInput components to update the VectoredMovement component. This is an important difference from how you are probably used to doing things in Unity. The PlayerInputProcessor does not know how the information it is generating will be used, and the ShipProcessor does not know where the information it is using came from. All the places you would normally have one component call a method on another component are instead replaced with reading from and writing to components. This means all the state of your application is right there in your components and you can pause Unity, click around and see exactly what's going on. The first time you have a puzzling error in your application and you simply pause Unity, go find the offending component, see one or more values that are clearly incorrect and make an easy fix, you will be sold on this idea. We have gone to great lengths to ensure that all of a RobotArms application's data can be inspected via the Unity editor. Alright, on to the final processor, time to bring this all together!</p>
<pre><code class="language-cs python">using UnityEngine;
using RobotArms;

[ProcessorOptions(typeof(Ship), typeof(PlayerInput), typeof(VectoredMovement))]
public <span class="class"><span class="keyword">class</span> <span class="title">ShipMovementProcessor</span> :</span> RobotArmsProcessor {
    public override void Process (GameObject entity) {
        var input = entity.GetComponent&lt;PlayerInput&gt;();
        var ship = entity.GetComponent&lt;Ship&gt;();
        var movement = entity.GetComponent&lt;VectoredMovement&gt;();

        <span class="keyword">if</span> (ship.Fuel &gt; <span class="number">0</span>) {
            <span class="keyword">if</span> (input.Thrust) {
                movement.Velocity += entity.transform.up * ship.ThrustForce * Time.deltaTime;
                ship.Fuel -= ship.FuelConsumptionPerSecond * Time.deltaTime;
            }

            entity.transform.Rotate(<span class="number">0</span>, <span class="number">0</span>, -input.Rotation * ship.RotationSpeed * Time.deltaTime);
        }
    }
}</code></pre>
<p>Here we have an example of a processor that requires 3 different components. Only a GameObject that has all three of these components will be passed in to this processor, so asteroids floating around with VectoredMovement components will never show up here, nor will enemy ships with Ship and VectoredMovement components but not a PlayerInput component. Add the Ship component to your ship GameObject, set some values for the rotation speed, fuel, fuel consumption rate, and thrust force and hit play.</p>
<p>So there we go. It's not a huge project but hopefully this has given you a taste of how RobotArms is different and perhaps how it can make your Unity development more productive.</p>
<h3>License</h3>
<pre>This project is licensed under The MIT License (MIT)

Copyright 2014 David Koontz, Trenton Kennedy

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.

Please direct questions, patches, and suggestions to the project page at
https://bitbucket.org/dkoontz/robotarms
</pre>

<h3>External Projects</h3>
<p>The demo project uses sprites provided by Kenney Vleugels (www.kenney.nl)<br><a href="http://opengameart.org/content/space-shooter-redux">http://opengameart.org/content/space-shooter-redux</a></p>
<h3>Contact</h3>
<p>Bugs, comments, complaints should be directed to the <a href="https://bitbucket.org/dkoontz/robotarms">RobotArms repository</a></p>
<p>Or contact the authors directly:<br>David Koontz - david@koontzfamily.org<br>Trenton Kennedy - trentonkennedy@gmail.com</p>
</body></html>